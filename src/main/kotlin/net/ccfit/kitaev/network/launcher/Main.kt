package net.ccfit.kitaev.network.launcher

import net.ccfit.kitaev.network.data.ProgramOptions
import net.ccfit.kitaev.network.main.KeyboardHandler
import net.ccfit.kitaev.network.main.Node

fun main(args: Array<String>) {
    val programOptions = parseOptions(args) ?: return
    KeyboardHandler().apply {
        startNode(
                this,
                programOptions.lostPercent,
                programOptions.nodeName,
                programOptions.nodePort,
                programOptions.parentIp,
                programOptions.parentPort
        )

        start()
    }
}

private fun startNode(
        keyboardHandler: KeyboardHandler,
        packetLost: Int,
        nodeName: String,
        nodePort: Int,
        parentIp: String? = null,
        parentPort: Int? = null
) {
    val node = Node(packetLost, nodeName, nodePort, parentIp, parentPort)
    keyboardHandler.appendNode(node)
    node.start()
}

private fun parseOptions(args: Array<String>): ProgramOptions? {
    if (args.size < 3) {
        println("Example call: <Lost percent> <Node name> <Node port> <Parent ip?> <Parent port?>")
        return null
    }

    val programOptions = ProgramOptions()
    programOptions.lostPercent = args[0].toInt()
    programOptions.nodeName = args[1]
    programOptions.nodePort = args[2].toInt()

    if (args.size >= 5) {
        programOptions.parentIp = args[3]
        programOptions.parentPort = args[4].toInt()
    }

    return programOptions
}