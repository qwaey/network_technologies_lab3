package net.ccfit.kitaev.network.domain

import net.ccfit.kitaev.network.data.Connection

class TimeoutDemon(nodeName: String, private val connections: MutableMap<Connection, TaskHeap>) : Thread("$nodeName connection demon") {

    override fun run() {
        while (Thread.currentThread().isAlive) {
            sleep(1000)
            val iterator = connections.iterator()
            while (iterator.hasNext()) {
                val entry = iterator.next()
                entry.value.timeout--
                if (entry.value.timeout <= 0) {
                    println("${entry.value.nodeName} disconnected")
                    iterator.remove()
                }
            }
        }
    }
}