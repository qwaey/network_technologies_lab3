package net.ccfit.kitaev.network.domain

import net.ccfit.kitaev.network.data.PacketMeta
import net.ccfit.kitaev.network.data.PacketType
import net.ccfit.kitaev.network.data.SharedConstants
import java.net.DatagramPacket
import java.nio.charset.StandardCharsets.UTF_8

class PacketFactory {

    companion object {

        fun getPacket(packetMeta: PacketMeta): DatagramPacket {
            val data = generatePacketData(packetMeta)
            return DatagramPacket(data, data.size, packetMeta.inetAddress, packetMeta.port)
        }

        fun getPacketMeta(packet: DatagramPacket): PacketMeta? {
            val inet = packet.address
            val port = packet.port

            val strings = String(packet.data, UTF_8).split(SharedConstants.MESSAGE_DELIMETR)

            var node = ""
            var type = PacketType.PING
            var uuid = ""
            var message = ""

            try {
                when (strings.size) {
                    1 -> { // PING OR QUIT
                        type = PacketType.typeOf(strings[0].toInt())
                    }

                    3 -> { // HELLO OR ANSWER
                        type = PacketType.typeOf(strings[0].toInt())
                        uuid = strings[1]
                        node = strings[2]
                    }

                    4 -> { // MESSAGE
                        type = PacketType.typeOf(strings[0].toInt())
                        uuid = strings[1]
                        node = strings[2]
                        message = strings[3]
                    }

                    else -> throw RuntimeException("Received broken packet $strings")
                }

                return PacketMeta(node, type, inet, port, 0, uuid, message)
            } catch (e: Exception) {
                e.localizedMessage
                return null
            }
        }

        private fun generatePacketData(packetMeta: PacketMeta): ByteArray {
            return when (packetMeta.packetType) {
                PacketType.HELLO -> getType(packetMeta) + getUUID(packetMeta) + getNodeName(packetMeta) + getMessage(packetMeta)
                PacketType.PING -> getType(packetMeta)
                PacketType.MESSAGE -> getType(packetMeta) + getUUID(packetMeta) + getNodeName(packetMeta) + getMessage(packetMeta)
                PacketType.ANSWER -> getType(packetMeta) + getUUID(packetMeta) + getNodeName(packetMeta)
                PacketType.QUIT -> getType(packetMeta)
            }
        }

        private fun getNodeName(packetMeta: PacketMeta): ByteArray {
            return SharedConstants.MESSAGE_DELIMETR.toByteArray() + packetMeta.nodeName.toByteArray()
        }

        private fun getType(packetMeta: PacketMeta): ByteArray {
            return packetMeta.packetType.toBytes()
        }

        private fun getUUID(packetMeta: PacketMeta): ByteArray {
            return packetMeta.uuid.toByteArray()
        }

        private fun getMessage(packetMeta: PacketMeta): ByteArray {
            return SharedConstants.MESSAGE_DELIMETR.toByteArray() + packetMeta.message.toByteArray()
        }

        private fun PacketType.toBytes(): ByteArray {
            return this.ordinal.toString().toByteArray() + SharedConstants.MESSAGE_DELIMETR.toByteArray()
        }
    }
}