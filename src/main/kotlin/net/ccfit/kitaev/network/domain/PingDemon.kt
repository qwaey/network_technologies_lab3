package net.ccfit.kitaev.network.domain

import net.ccfit.kitaev.network.data.Connection
import java.net.DatagramSocket

class PingDemon(
        private val nodeName: String,
        private val socket: DatagramSocket,
        private val ip: String,
        private val port: Int
) : Thread("$nodeName ping demon") {

    override fun run() {
        val packetMeta = TaskFactory(nodeName).getPingTask(Connection(ip, port))
        val packet = PacketFactory.getPacket(packetMeta)

        while (Thread.currentThread().isAlive) {
            sleep(500)
            socket.send(packet)
        }
    }
}