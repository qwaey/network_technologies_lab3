package net.ccfit.kitaev.network.domain

import net.ccfit.kitaev.network.data.Connection
import net.ccfit.kitaev.network.data.PacketMeta
import net.ccfit.kitaev.network.data.PacketType
import net.ccfit.kitaev.network.data.SharedConstants
import java.net.InetAddress

class TaskFactory(
        private val myNodeName: String
) {

    fun getOtherHelloTask(connection: Connection, uuid: String, nodeName: String): PacketMeta {
        val inet = InetAddress.getByName(connection.ip)
        return PacketMeta(
                nodeName,
                PacketType.HELLO,
                inet,
                connection.port,
                SharedConstants.SAFE_AMOUNT_SEND,
                uuid
        )
    }

    fun getHelloTask(connection: Connection, uuid: String): PacketMeta {
        val inet = InetAddress.getByName(connection.ip)
        return PacketMeta(
                myNodeName,
                PacketType.HELLO,
                inet,
                connection.port,
                SharedConstants.SAFE_AMOUNT_SEND,
                uuid
        )
    }

    fun getPingTask(connection: Connection): PacketMeta {
        val inet = InetAddress.getByName(connection.ip)
        return PacketMeta(
                myNodeName,
                PacketType.PING,
                inet,
                connection.port,
                SharedConstants.PING_COUNT_SEND
        )
    }

    fun getMessageTask(connection: Connection, uuid: String, message: String): PacketMeta {
        val inet = InetAddress.getByName(connection.ip)
        return PacketMeta(
                myNodeName,
                PacketType.MESSAGE,
                inet,
                connection.port,
                SharedConstants.SAFE_AMOUNT_SEND,
                uuid,
                message
        )
    }

    fun getOtherMessageTask(connection: Connection, uuid: String, message: String, nodeName: String): PacketMeta {
        val inet = InetAddress.getByName(connection.ip)
        return PacketMeta(
                nodeName,
                PacketType.MESSAGE,
                inet,
                connection.port,
                SharedConstants.SAFE_AMOUNT_SEND,
                uuid,
                message
        )
    }

    fun getAnswerTask(connection: Connection, uuid: String): PacketMeta {
        val inet = InetAddress.getByName(connection.ip)
        return PacketMeta(
                myNodeName,
                PacketType.ANSWER,
                inet,
                connection.port,
                1,
                uuid
        )
    }

    fun getQuitTask(connection: Connection): PacketMeta {
        val inet = InetAddress.getByName(connection.ip)
        return PacketMeta(
                myNodeName,
                PacketType.QUIT,
                inet,
                connection.port,
                SharedConstants.PING_COUNT_SEND
        )
    }
}