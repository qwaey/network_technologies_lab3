package net.ccfit.kitaev.network.domain

import net.ccfit.kitaev.network.data.PacketMeta
import net.ccfit.kitaev.network.data.PacketType
import net.ccfit.kitaev.network.data.SharedConstants
import java.net.DatagramSocket

class TaskHeap(val nodeName: String, socket: DatagramSocket, ip: String, port: Int) {

    var timeout = SharedConstants.TIMEOUT_IN_SECOND
    private val toDoList = mutableMapOf<String, PacketMeta>()
    private val pingDemon = PingDemon(nodeName, socket, ip, port).apply {
        this.start()
    }

    fun resolve(socket: DatagramSocket) {
        val iterator = toDoList.iterator()
        while (iterator.hasNext()) {
            val (_, meta) = iterator.next()

            socket.send(PacketFactory.getPacket(meta))

            if (meta.timeToDeath == 0) {
                removeHandler(meta)
                iterator.remove()
            }

            meta.timeToDeath--
        }
    }

    fun addTask(uuid: String, packetMeta: PacketMeta) {
        toDoList[uuid] = packetMeta
    }

    fun removeTask(uuid: String) {
        toDoList.remove(uuid)
    }

    fun interrupt() {
        pingDemon.interrupt()
    }

    private fun removeHandler(packetMeta: PacketMeta) {
        when (packetMeta.packetType) {
            PacketType.QUIT -> println("Node ${packetMeta.nodeName} disconnected")
            PacketType.HELLO -> println("Node ${packetMeta.nodeName} is not available")
            PacketType.MESSAGE -> println("Node ${packetMeta.nodeName} is no longer available")
            else -> return
        }

        toDoList.clear()
    }
}