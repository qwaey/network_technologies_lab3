package net.ccfit.kitaev.network.main

import net.ccfit.kitaev.network.data.Connection
import net.ccfit.kitaev.network.domain.TaskFactory
import net.ccfit.kitaev.network.domain.TaskHeap
import java.net.DatagramSocket
import java.util.UUID

class NodeSender(
        private val nodeName: String,
        private val socket: DatagramSocket,
        private val connections: MutableMap<Connection, TaskHeap>
) : Thread("$nodeName receiver") {

    private val taskFactory = TaskFactory(nodeName)

    override fun run() {
        try {
            while (Thread.currentThread().isAlive) {
                val iterator = connections.values.iterator()
                while (iterator.hasNext()) {
                    iterator.next().resolve(socket)
                }
            }

            connections.forEach {
                it.value.addTask(UUID.randomUUID().toString(), taskFactory.getQuitTask(it.key))
                it.value.resolve(socket)
                it.value.interrupt()
            }
        } catch (e: Exception) {
            e.localizedMessage
        }
    }

    fun appendNode(connection: Connection) {
        connections[connection] = TaskHeap(nodeName, socket, connection.ip, connection.port)
    }

    fun sendMessage(message: String, noConnect: Connection? = null) {
        connections.forEach { connection, taskHeap ->
            if (noConnect == null || connection != noConnect) {
                val uuid = UUID.randomUUID().toString()
                taskHeap.addTask(uuid, taskFactory.getMessageTask(connection, uuid, message))
            }
        }
    }

    fun resendMessage(message: String, noConnect: Connection, node: String) {
        connections.forEach { connection, taskHeap ->
            if (connection != noConnect) {
                val uuid = UUID.randomUUID().toString()
                taskHeap.addTask(uuid, taskFactory.getOtherMessageTask(connection, uuid, message, node))
            }
        }
    }

    fun sendAnswer(connection: Connection, uuid: String) {
        connections[connection]?.addTask(uuid, taskFactory.getAnswerTask(connection, uuid))
    }

    fun resendHello(noConnect: Connection, node: String) {
        connections.forEach { connection, taskHeap ->
            if (connection != noConnect) {
                val uuid = UUID.randomUUID().toString()
                taskHeap.addTask(uuid, taskFactory.getOtherHelloTask(connection, uuid, node))
            }
        }
    }
}