package net.ccfit.kitaev.network.main

import net.ccfit.kitaev.network.data.SharedConstants

class KeyboardHandler : Thread("KeyboardHandler") {

    private val nodeMap = mutableMapOf<String, Node>()

    override fun run() {
        println("Message keyboard input form:\n<Node name>: <message>")

        try {
            while (Thread.currentThread().isAlive) {
                val inputMessage = readLine()!!
                val (nodeName, message) = inputMessage.portSplit(':')
                nodeMap[nodeName] ?: println("Bad node name")

                if (message.length > SharedConstants.PACKET_SIZE) {
                    println("Message so big")
                    continue
                }

                if (message.contains(SharedConstants.MESSAGE_DELIMETR)) {
                    println("Message must not contains newline character")
                    continue
                }

                handleStopEvent(nodeName, message)
                nodeMap[nodeName]?.sendMessage(message)
            }
        } catch (e: Exception) {
            println("Wrong keyboard input")
        }
    }

    fun appendNode(node: Node) {
        nodeMap[node.nodeName] = node
    }

    private fun handleStopEvent(nodeName: String, message: String) {
        if (message == "exit") {
            nodeMap[nodeName]?.interrupt()
            nodeMap.remove(nodeName)
            if (nodeMap.isEmpty()) {
                Thread.currentThread().interrupt()
            }
        }
    }

    private fun String.portSplit(delimiter: Char): Pair<String, String> {
        var nodeName = ""
        var message = ""

        for (pos in 0 until this.length) {
            if (this[pos] == delimiter) {
                nodeName = this.substring(0 until pos)

                if (pos + 2 < this.length) {
                    message = this.substring((pos + 2) until this.length)
                }
            }
        }

        return nodeName to message
    }
}

