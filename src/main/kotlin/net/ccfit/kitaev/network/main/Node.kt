package net.ccfit.kitaev.network.main

import net.ccfit.kitaev.network.data.*
import net.ccfit.kitaev.network.domain.TimeoutDemon
import net.ccfit.kitaev.network.domain.PacketFactory
import net.ccfit.kitaev.network.domain.TaskFactory
import net.ccfit.kitaev.network.domain.TaskHeap
import java.net.DatagramPacket
import java.net.DatagramSocket
import java.util.Random
import java.util.UUID

class Node(
        private val lostPercent: Int,
        val nodeName: String,
        nodePort: Int,
        private val parentIp: String? = null,
        private val parentPort: Int? = null
) : Thread("$nodeName receiver") {

    private val taskFactory = TaskFactory(nodeName)
    private val packet = DatagramPacket(ByteArray(SharedConstants.PACKET_SIZE), SharedConstants.PACKET_SIZE)
    private val socket = DatagramSocket(nodePort)
    private val connections = mutableMapOf<Connection, TaskHeap>()
    private val nodeSender = NodeSender(nodeName, socket, connections)
    private val connectionDemon = TimeoutDemon(nodeName, connections).apply {
        this.start()
    }

    override fun run() {
        if (parentIp != null && parentPort != null) {
            val connection = Connection(parentIp, parentPort)
            val uuid = UUID.randomUUID().toString()
            nodeSender.appendNode(connection)
            connections[connection]?.addTask(uuid, taskFactory.getHelloTask(connection, uuid))
        }

        nodeSender.start()

        while (Thread.currentThread().isAlive) {
            socket.receive(packet)

            if (Random().nextInt(100) < lostPercent) {
                // Lost packet
                continue
            }

            val packetMeta = PacketFactory.getPacketMeta(packet)
            packetMetaHandler(packetMeta)
        }

        connectionDemon.interrupt()
        nodeSender.interrupt()
    }

    fun sendMessage(message: String) {
        nodeSender.sendMessage(message)
    }

    private fun packetMetaHandler(packetMeta: PacketMeta?) {
        if (packetMeta == null) {
            return
        }

        val connection = Connection(packetMeta.inetAddress.hostAddress, packetMeta.port)
        when (packetMeta.packetType) {
            PacketType.PING -> {
                if (connections.contains(connection)) {
                    connections[connection]?.timeout = SharedConstants.TIMEOUT_IN_SECOND
                }
            }

            PacketType.QUIT -> connections[connection]?.timeout = 0

            PacketType.ANSWER -> {
                connections[connection]?.timeout = SharedConstants.TIMEOUT_IN_SECOND
                connections[connection]?.removeTask(packetMeta.uuid)
            }

            PacketType.HELLO -> {
                nodeSender.appendNode(connection)
                MessagePrinter.print(packetMeta.uuid, packetMeta.nodeName, "connected")
                nodeSender.sendAnswer(connection, packetMeta.uuid)
                nodeSender.resendHello(connection, packetMeta.nodeName)
            }

            PacketType.MESSAGE -> {
                if (connections.contains(connection)) {
                    connections[connection]?.timeout = SharedConstants.TIMEOUT_IN_SECOND
                    MessagePrinter.print(packetMeta.uuid, packetMeta.nodeName, packetMeta.message)
                    nodeSender.resendMessage(packetMeta.message, connection, packetMeta.nodeName)
                    connections[connection]?.addTask(packetMeta.uuid, taskFactory.getAnswerTask(connection, packetMeta.uuid))
                }
            }
        }
    }
}