package net.ccfit.kitaev.network.main

import net.ccfit.kitaev.network.data.SharedConstants
import java.util.*

class MessagePrinter {

    companion object {
        private val messageQueue = ArrayDeque<String>()

        fun print(uuid: String, author: String, message: String) {
            if (messageQueue.contains(uuid)) {
                return // ignore message
            }

            if (messageQueue.size > SharedConstants.QUEUE_CAPACITY) {
                messageQueue.removeFirst()
            }

            messageQueue.addLast(uuid)
            println("$author: $message")
        }
    }
}