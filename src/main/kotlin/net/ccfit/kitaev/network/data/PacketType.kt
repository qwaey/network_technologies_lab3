package net.ccfit.kitaev.network.data

enum class PacketType {
    HELLO,
    MESSAGE,
    PING,
    ANSWER,
    QUIT;

    companion object {
        fun typeOf(ordinal: Int): PacketType {
            return when (ordinal) {
                0 -> HELLO
                1 -> MESSAGE
                2 -> PING
                3 -> ANSWER
                4 -> QUIT
                else -> throw UnsupportedOperationException("Invalid ordinal")
            }
        }
    }
}