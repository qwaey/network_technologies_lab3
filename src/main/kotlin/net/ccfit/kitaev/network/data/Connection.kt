package net.ccfit.kitaev.network.data

data class Connection(val ip: String, val port: Int)