package net.ccfit.kitaev.network.data

data class ProgramOptions(
        var lostPercent: Int = 0,
        var nodeName: String = "Undefined node name",
        var nodePort: Int = 0,
        var parentIp: String? = null,
        var parentPort: Int? = null
)