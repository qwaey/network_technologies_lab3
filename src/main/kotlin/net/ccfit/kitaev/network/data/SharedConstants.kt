package net.ccfit.kitaev.network.data

class SharedConstants {

    companion object {
        const val PACKET_SIZE = 65507 /* MAX_UDP_PACKET_SIZE */
        const val SAFE_AMOUNT_SEND = 200
        const val PING_COUNT_SEND = -1 /* == infinity :) */
        const val QUEUE_CAPACITY = 500
        const val TIMEOUT_IN_SECOND = 10
        const val MESSAGE_DELIMETR = "\n"
    }
}