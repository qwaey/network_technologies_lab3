package net.ccfit.kitaev.network.data

import java.net.InetAddress

data class PacketMeta(
        val nodeName: String,
        val packetType: PacketType,
        val inetAddress: InetAddress,
        val port: Int,
        var timeToDeath: Int,
        val uuid: String = "",
        val message: String = ""
)